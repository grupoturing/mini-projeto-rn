import datasets
from rede_neural import RedeNeural
from camada import Camada
import matplotlib.pyplot as plt

#####################################
####### COMPLETE ESTE CÓDIGO ########
#####################################

# Modifique as saidas para que elas possam ser processadas pela rede neural.
# Ex: um parâmetro cor (que pode ser vermelho, azul ou verde) devevirar 3
# parâmetros, um para cada cor.
#     vermelho = [1, 0, 0]
#         azul = [0, 1, 0]
#        verde = [0, 0, 1]

# Experimente com outros datasets, além do iris. O dataset Red Wine Quality
# pode ser carregado com a função datasets.wine(),
# de modo análogo a datasets.le_iris().

# Modifique a taxa de aprendizado e as camadas da rede neural.

(entradas_treino, saidas_treino), (entradas_teste, saidas_teste) = \
    datasets.le_iris()

rn = RedeNeural([
    Camada(4, 5),
    Camada(5, 5),
    Camada(5, 3),
])

TAXA_APRENDIZADO = .1
EPOCAS = 2000

custos_treino = []
custos_teste = []

for i in range(EPOCAS):
    custo_treino = 0
    for entrada, saida in zip(entradas_treino, saidas_treino):
        custo_treino += rn.treina(entrada, saida, TAXA_APRENDIZADO)
    custo_treino /= len(entradas_treino)
    custos_treino.append(custo_treino)

    custo_teste = 0
    for entrada, saida in zip(entradas_teste, saidas_teste):
        saida_real = rn.feedforward(entrada)
        custo_teste += rn.custo(saida_real, saida)
    custo_teste /= len(entradas_teste)
    custos_teste.append(custo_teste)

    if i % 100 == 0:
        print(f'Epoca {i: <9d}'
              f'custo_treino={custo_treino: <11.3f}'
              f'custo_teste={custo_teste:.3f}')
        print('Saída esperada', *[f'{x:.3f}' for x in saida])
        print('Saída real    ', *[f'{x:.3f}' for x in saida_real])
        print()

plt.plot(range(len(custos_treino)), custos_treino, label='Treino')
plt.plot(range(len(custos_teste)), custos_teste, label='Teste')
plt.legend()
plt.xlabel('Época')
plt.ylabel('Custo')
plt.show()
