import random
import math


class Camada:
    def __init__(self, num_entradas, num_saidas):
        '''Inicializa uma camada de uma rede neural com pesos e vieses aleatórios.

        Recebe:
            num_entradas: o número de entradas, ou seja, o número de neurônios
                          da camada anterior.
            num_saidas: o número de neurônios dessa camada.

        Sugestão de atributos:
            self.pesos: uma matriz de dimensões (num_saidas, num_entradas). Ou
                        seja, uma lista de neurônios, onde cada neurônio é uma
                        lista com (num_entrada) pesos.
            self.vieses: uma lista de float com (num_saídas) elementos, um
                         para cada neurônio.
            self.ultimas_ativacoes: lista com as últimas ativações dos
                                    neurônios dessa rede.
            self.ultimas_derivadas_ativacoes: lista com a derivada da função
                                 de ativação avaliada em cada um dos neurônios.
            self.deltas: lista com os últimos deltas dessa rede
                       (é atualizado pela função RedeNeural.backpropagation()).
        '''
        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################

    def ativacoes(self, entradas):
        '''Calcula as ativações dos neurônios dessa camada.

        Sugestão: use essa função para atualizar os atributos
                  self.ultimas_ativacoes e self.ultimas_derivadas_ativacoes.

        Recebe:
            entradas: uma lista de floats representando as entradas, ou seja,
                      as ativações dos neurônios da camada anterior.

        Retorna:
            uma lista com as ativações dos neurônios dessa camada.
        '''
        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################

        # Dica: a derivada da função sigmóide é sigmoide(x) * (1-sigmoide(x))

        return self.ultimas_ativacoes

    def funcao_ativacao(self, x):
        '''Calcula a funcao de ativação em x.

        Recebe:
            x: float.

        Retorna:
            O valor da função de ativação em x.
        '''
        # Se for usar a função de ativação sigmóide, note que,
        # para valores muito pequenos de x, math.exp(-x) causa um
        # OverflowError.
        if x < -200:
            return 0

        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################
