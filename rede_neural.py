class RedeNeural:
    def __init__(self, camadas):
        '''Inicializa uma rede neural.

        Recebe:
            camadas: lista de objetos de tipo Camada.

        Sugestão de atributos:
            self.camadas: lista com as camadas dessa rede.
        '''
        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################

    def feedforward(self, entradas):
        '''Calcula a saída de uma rede neural.

        Recebe:
            entradas: lista de float com as entradas da rede.

        Retorna:
            Lista de float com as saídas da rede.
        '''
        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################

    def custo(self, saidas, saidas_esperadas):
        '''Calcula a função custo.

        Recebe:
            saidas: lista de float com as saídas da rede.
            saidas_esperadas: lista de float com as saídas esperadas.

        Retorna:
            float: o valor da função custo para as saídas especificadas.
        '''
        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################

    def backpropagation(self, saidas_esperadas):
        '''Realiza o backpropagation.

        Isto é, calcula os deltas de cada camada e os armazena em
        camada.deltas.

        IMPORTANTE: Essa função ocorre após o feedforward, ou seja, utiliza as
            ativações e outros valores já calculados. Essa função não deve
            chamar self.feedforward() ou camada.ativacoes().

        Recebe:
            saidas_esperadas: lista de float com as saídas esperadas da rede.
        '''
        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################

    def gradient_descent(self, entradas, taxa_aprendizado=.1):
        '''Realiza gradient descent.

        Isto é, utiliza os deltas e ativações já calculados para atualizar os
        pesos e vieses das camadas dessa rede.

        IMPORTANTE: Essa função ocorre após o feedforward, ou seja, utiliza as
            ativações e outros valores já calculados. Essa função não deve
            chamar self.feedforward() ou camada.ativacoes().

        Recebe:
            entradas: lista de float com as entradas da rede.
            taxa_aprendizado: float.
        '''
        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################

    def treina(self, entradas, saidas_esperadas, taxa_aprendizado=.1):
        '''Treina a rede.

        Para treinar a rede, é necessário realizar o feedforward,
        backpropagation e gradient descent.

        Recebe:
            entradas: lista de float com as entradas da rede.
            saidas_esperadas: lista de float com as saídas esperadas da rede.
            taxa_aprendizado: float.

        Retorna:
            float: o custo da rede para as entradas dadas.
        '''
        #####################################
        ####### COMPLETE ESTE CÓDIGO ########
        #####################################
