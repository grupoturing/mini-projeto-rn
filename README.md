# Mini Projeto - Redes Neurais

### Progressão sugerida:

#### Semana 1: feed forward

1. Implementar as funções `Camada.__init__` e `Camada.funcao_ativacao` (em camada.py).
2. Implementar `Camada.ativacoes` (em camada.py).
3. Implementar `RedeNeural.__init__` e `RedeNeural.feedforward` (em rede_neural.py).

#### Semana 2: backpropagation

1. Implementar, nessa ordem, em rede_neural.py:
    1. `RedeNeural.custo`.
    2. `RedeNeural.backpropagation`.
    3. `RedeNeural.gradient_descent`
    4. `RedeNeural.treina`
2. Completar o arquivo teste.py.

*Você pode criar outras funções e atributos que julgar necessários.*

### Datasets

- [Iris Species - UCI Machine Learning - Kaggle](https://www.kaggle.com/uciml/iris)

- [Red Wine Quality - UCI Machine Learning - Kaggle](https://www.kaggle.com/uciml/red-wine-quality-cortez-et-al-2009)
