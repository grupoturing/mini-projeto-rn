import csv
import random


def to_float(matriz):
    '''Converte uma matriz para float.
    
    Args:
        matriz: uma matriz (uma lista de listas)
    
    Returns:
        Uma matriz de float.
    '''

    return [[float(val) for val in linha] for linha in matriz]


def le_csv(arquivo):
    '''Lê um arquivo csv.

    Args:
        arquivo: o nome do arquivo.

    Returns:
        uma lista de dados,
        onde cada dado é uma lista de float (uma linha do csv).
    '''

    dados = []
    with open(arquivo) as f:
        reader = csv.reader(f)
        for linha in reader:
            dados.append(linha)
    return dados


def le_iris():
    '''Le o dataset Iris.

    Retorna:
        (entradas_treino, saidas_treino), (entradas_teste, saidas_teste)
    '''

    iris = le_csv('datasets/Iris.csv')[1:]  # Primeira linha é o cabeçalho
    random.shuffle(iris)

    entradas = to_float([linha[1:-1] for linha in iris])
    saidas = [linha[-1] for linha in iris]

    entradas_treino = entradas[:100]
    saidas_treino = saidas[:100]
    entradas_teste = entradas[100:]
    saidas_teste = saidas[100:]
    return (entradas_treino, saidas_treino), (entradas_teste, saidas_teste)


def le_wine():
    '''Le o dataset Red Wine Quality.

    Retorna:
        (entradas_treino, saidas_treino), (entradas_teste, saidas_teste)
    '''
    wine = to_float(le_csv('datasets/winequality-red.csv')
                    [1:])  # Primeira linha é o cabeçalho
    random.shuffle(wine)

    entradas = [linha[:-1] for linha in wine]
    saidas = [linha[-1] for linha in wine]

    entradas_treino = entradas[:1200]
    saidas_treino = saidas[:1200]
    entradas_teste = entradas[1200:]
    saidas_teste = saidas[1200:]
    return (entradas_treino, saidas_treino), (entradas_teste, saidas_teste)
